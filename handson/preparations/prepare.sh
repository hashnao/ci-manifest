#!/bin/bash

if test $# != 4;then
 echo "prepare.sh <apiserver> <admin-user> <admin-pass> <handson-usernum>"
 exit
fi

APISERVER=$1
ADMINUSER=$2
ADMINPASS=$3
USERNUM=$4
HANDSON_NAMESPACE="handson"

oc login -u ${ADMINUSER} -p ${ADMINPASS} ${APISERVER}

oc new-project $HANDSON_NAMESPACE

oc process postgresql-persistent --param-file=etherpad-db-template-param.txt --labels=app=etherpad_db -n openshift | oc apply -f -
oc process -f https://raw.githubusercontent.com/wkulhanek/docker-openshift-etherpad/master/etherpad-template.yaml --param-file=etherpad-template-param.txt | oc apply -f -
oc new-app --name='httpd' httpd~./text
sleep 60
oc start-build httpd --from-dir=./text
oc expose svc httpd

oc apply -f web-terminal-operator-subs.yaml

for userid in $(seq ${USERNUM}); do
    echo user$userid
    oc login -u user${userid} -p openshift $(oc whoami --show-server)
    users+=("user${userid}")
done

oc login -u ${ADMINUSER} -p ${ADMINPASS} ${APISERVER}

oc adm groups new handson-cluster-admins "${users[@]}"
oc adm policy add-cluster-role-to-group cluster-admin handson-cluster-admins --rolebinding-name=handson-cluster-admins

cd ../../demo
for userid in $(seq ${USERNUM}); do
    echo user$userid
    oc login -u user${userid} -p openshift $(oc whoami --show-server)
    ./demo.sh install -p user${userid}
done

oc login -u ${ADMINUSER} -p ${ADMINPASS} ${APISERVER}

cd ../handson/preparations
oc apply -f web-terminal-tooling-devworkspacetemplate.yaml
