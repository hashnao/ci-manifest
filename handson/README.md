# Starter Kit ハンズオン

Container Stater Kitで実施するハンズオンの手順を紹介します。

![overview](./img/handson-overview.png)
## 0. 環境への接続
---
インストラクターから提示されたOpenShiftクラスタのURLを開き、自身のユーザーでログインして下さい。  
ログイン画面で Identity Provider の選択肢が複数表示される場合は`htpasswd_provider`を選択してください。

ログイン後、以下の手順で Web Terminal を起動してください。
![webterminal](./img/web-terminal.png)

## 1. パイプラインの作成
---
![pipelines](./img/pipelines-cr.png)

### 1-1. リポジトリのクローン
---
OpenShiftクラスタに対しocコマンドが実行可能な環境で以下を実行し、必要なリポジトリをクローンします。
```
# 環境変数の設定
export USER=$(oc whoami)

# マニフェストリポジトリ
git clone http://gitea-${USER}-develop.apps.cluster-82dcv.82dcv.sandbox784.opentlc.com/gitea/starter-kit-cicd-manifest

# アプリケーションリポジトリ
git clone http://gitea-${USER}-develop.apps.cluster-82dcv.82dcv.sandbox784.opentlc.com/gitea/starter-kit-cicd-app
```

### 1-2. 実行環境の準備
---
パイプラインを実行するために必要な設定等を行います。
```
# 作業を行うProjectの作成
oc new-project ci-${USER}
```

### 1-3. Taskの作成
---
Pipelineの作成に必要なTaskを自身のProjectにデプロイします。
```
cd /home/user/starter-kit-cicd-manifest/handson/tasks/
oc apply -f .

# 確認
oc get task
---
NAME                AGE
git-clone           9s
sonarqube-scanner   9s
trivy               9s
```
通常のTaskはNamespace-scopedなカスタムリソースですが、クラスタ全体で利用可能なClusterTaskというカスタムリソースも存在します。  
OpenShiftではOpenShift Pipelines Operatorをインストールすると、デフォルトで複数のClusterTaskが作成されすぐに利用することができます。

```
oc get clustertask
---
NAME                       AGE
buildah                    17m
buildah-1-6-0              17m
git-cli                    17m
git-clone                  17m
...
```

### 1-4. Pipelineの作成
---
インストールしたTaskを使いPipelineを作成します。  
/handson/pipelinesディレクトリにあるhandson-pipeline.yamlファイルをエディタで修正していきましょう。
```
cd /home/user/starter-kit-cicd-manifest/handson/pipelines/

# エディタで編集（お好みのエディタをご使用下さい）
vim handson-pipeline.yaml
---
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: handson-pipeline
spec:
...
  # XXXXXXXXXXXとなっている部分に値を入力していく
  tasks:
  - name: git-clone-health
    taskRef:
      name: XXXXXXXXXXX
    params:
    - name: XXXXXXXXXXX
      value: $(params.git-url)
    - name: XXXXXXXXXXX
      value: $(params.git-revision)
    workspaces:
    - name: XXXXXXXXXXX
      workspace: shared-workspace
...
```



#### **Pipeline作成のヒント**
1-3でインストールしたそれぞれのTaskの中身を確認し、実行に必要なパラメーターやWorkspacesについて確認しましょう。
![tips](./img/pipeline-tips.png)
また、この READMEの冒頭にあるハンズオン概要図から実行すべきTaskの順番を確認しましょう。

進めるのが難しいと感じたら/handson/answers以下の回答を確認してください。

handson-pipeline.yamlを作成できたらクラスタにデプロイします。
```
cat handson-pipeline.yaml | envsubst |oc apply -f -

# 確認 
oc get pipeline
---
NAME               AGE
handson-pipeline   7s
```

### 1-5. PipelineRunの作成
---
1-4で作成したPipelineが正しく動作するかPipelineRunを作成して確認を行います。
/handson/pipelinesディレクトリにあるhandson-pipelinerun.yamlファイルを先ほどと同様にエディタで修正していきます。
```
vim handson-pipelinerun.yaml
```
こちらも/handson/answers以下に回答を用意しています。

handson-pipelinerun.yamlを作成できたらクラスタにデプロイしていきましょう。

今回はPipeline実行の中でアプリケーションリポジトリからコードをクローンするため、Giteaの認証情報をSecretとして作成し、`pipeline`というServeiceAccountに紐づけます。
```
cat git-auth.yaml | envsubst | oc apply -f -
oc secrets link pipeline git-creds
```

Workspaceとして使用するPVを作成し、その後PipelineRunを実行します。
```
oc apply -f tekton-pvc.yaml

# PipelineRunのマニフェストを実行する場合、最後のコマンドが"oc create -f -"となるため注意
cat handson-pipelinerun.yaml | envsubst | oc create -f -
```

これで先ほど作成したPipelineが実行されました。  
OpenShiftのコンソールにログインし、自身のProjectでパイプラインが実行されていることを確認しましょう。
画面左側のパイプラインからパイプライン実行を選択すると実行状況を確認することができます。  
  
  
![tips](./img/pipeline-success.png)

無事にパイプラインが成功していることを確認できました。  
もし実行失敗となっている場合、ログやイベントから失敗原因を確認し、yamlファイルの修正を行った上であらためて実行してみましょう。  
  

  
## 2. トリガーの作成
---
ここからは作成したPipelineをGitリポジトリからのWebhookを受けて実行するため、
Tekton Triggersのカスタムリソースオブジェクトを作成していきます。
![triggers](./img/triggers-cr.png)

### 2-1. TriggerTemplateの作成
まず初めに/handson/triggersディレクトリに移動し、TriggerTemplateを作成します。
TriggerTemplateの中身はPipelineRunとほぼ同じですが、Webhookで受け取った値をパラメーターとして利用するための設定が追加されています。
```
cd /home/user/starter-kit-cicd-manifest/handson/triggers

# TriggerTemplateを編集
vim handson-template.yaml
```
編集が完了したらクラスタに対しデプロイします。
```
oc apply -f handson-template.yaml

# 確認
oc get triggertemplate
---
NAME               AGE
handson-template   23s
```

### 2-2. TriggerBindingの作成
続いてTriggerBindingを作成していきます。
こちらは編集箇所が無いためファイルの中身を確認し、そのままデプロイしましょう。
```
# ファイルの確認
less handson-binding.yaml

oc apply -f handson-binding.yaml

# 確認
oc get triggerbinding
---
NAME              AGE
handson-binding   12s
```
以下のGiteaのページを合わせて確認し、Webhookのどのパラメーターを取得しているか確認してみて下さい。  
https://docs.gitea.io/en-us/webhooks/

### 2-3. SecretとServiceAccountの作成
EventListenerを作成する前に、必要なSecretとServiceAccountを作成していきます。
まずWebhookのSecret keyとなるSecretを作成します。
```
# 任意の値を設定して下さい
export SECRET_TOKEN=Webhook用トークン
oc create secret generic git-webhook --from-literal=secretkey=${SECRET_TOKEN}
```

続いてServiceAccountを作成します。  
sample-sa.yamlではPipelineRun作成に必要なRBAC設定についても併せて実施しています。
```
cat sample-sa.yaml | envsubst |oc apply -f -
```  



### 2-4. EventListenerの作成
最後にEventListenerを作成します。
handson-listener.yamlファイルを修正していきましょう。
```
vim handson-listener.yaml
```
編集が完了したらクラスタに対しデプロイします。
```
oc apply -f handson-listener.yaml

# 確認
oc get eventlistener
---
NAME               ADDRESS                                                 AVAILABLE   REASON                     READY   REASON
handson-listener   http://el-handson-listener.xxx.svc.cluster.local:8080   True        MinimumReplicasAvailable   True   
```

EventListenerはデフォルトでは外部公開されていないため、Routeを作成します。
後ほどこのURLをWebhookの宛先として設定します。
```
# Routeの作成
oc expose service el-handson-listener

# 確認
oc get route
NAME                  HOST/PORT                                   PATH   SERVICES              PORT            TERMINATION   WILDCARD
el-handson-listener   el-handson-listener-xxx.openshiftapps.com          el-handson-listener   http-listener                 None
```
試しにブラウザからアクセスしてみましょう。（適宜URLを置き換えて下さい）  
http://el-handson-listener-xxx.openshiftapps.com

以下のような表示がされていればEventListenerが正しく動作しています。  
`{"eventListener":"handson-listener","namespace":"ci-<ユーザ名>","eventListenerUID":"","errorMessage":"Invalid event body format format: unexpected end of JSON input"}`

## 3. CIの実行
### 3-1. GitリポジトリでのWebhook設定
2-4で作成したEventListenerに対しWebhookを実行するようGitリポジトリの設定を行なっていきます。
ブラウザからForkしたアプリケーションリポジトリを開きましょう。

http://gitea-<ユーザ名>-develop.apps.cluster-82dcv.82dcv.sandbox784.opentlc.com/gitea/starter-kit-cicd-app

画面右上の`サインイン`からユーザ名、パスワードを入力してサインインします。ユーザ名は gitea、パスワードは openshift です。

画面右上の`Settings -> Webhook -> Add Webhook -> Gitea`からWebhookの設定を行います。
各入力項目に以下を設定しましょう。
```
Target URL: 2-4で作成したEventListenerのURL
Secret: 2-3で入力したトークンの値 
Push events: チェックを入れ、Branch filter に"main"と入力
```
設定が出来たら`Add webhook`ボタンをクリックします。
作成したWebhookにて、`Test Delivery`を実行すると、パイプラインが実行されます。
OpenShiftコンソールを開き、新たなパイプライン実行が作成されていることを確認してみましょう。

### 3-2. 開発の流れの中でのCI実行

ここまででCIを実行するために必要な設定を全て実施できました。  
最後に開発の流れの中でCIパイプラインがどのように実行されるか確認しておきましょう。

![githubflow](./img/githubflow.png)
今アプリケーションリポジトリはGitHub Flowで開発が行われ、mainとfeatureという2つのブランチが存在しています。  
開発者はfeatureで新機能の開発を行い、それをmainにマージしていきます。  

あらためてブラウザでアプリケーションリポジトリを開き、featureブランチを選択しましょう。
![feature](./img/feature-branch.png)


/site/public/index.html を選択し
画面右上のペンのマークから編集画面を開き、74行目以下のコメントアウトを外します。
```
# before
      <!--
      <div class="box">
        <div class="map" id='map'></div>
      </div>
      -->

# after
      <div class="box">
        <div class="map" id='map'></div>
      </div>
```
変更できたらCommitを実施しましょう。  
![commit](./img/commit.png)  

画面上の`Pull Requests -> New Pull Request`から新しいプルリクエストを作成します。
`pull from`の部分を`feature`に変更し`New Pull Request -> Create Pull Request`でプルリクエストを作成します。

![mr](./img/pullrequest.png)   
続けてマージリクエスト画面で`Merge Pull Request`ボタンを押しましょう。  
これでfeatureで追加した変更がmainに取り込まれるとともに、CIパイプラインが実行されました。  
OpenShiftコンソールからパイプラインの実行を確認しましょう。

以上で本ハンズオンは終了です。お疲れ様でした。


