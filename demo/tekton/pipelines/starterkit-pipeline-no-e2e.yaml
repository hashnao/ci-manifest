apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: starterkit-pipeline
spec:
  workspaces:
  - name: shared-workspace
    description: Git Repo for Patient Health Records Apps
  - name: manifest-workspace
    description: Git Repo for k8s manifest
  - name: sonar-settings
    description: Temporary dir for sonarqube-scan Task
  - name: vul-cache
    description: Cache vulnerabilities infomations
  - name: temp-dir
    description: Temporary dir for git-cli Task
  - name: aws-cred
    description: AWS credential for aws-cli Task
  params:
  - name: target-path
    type: string 
  #- name: imageurl
  #  type: string
  #  description: Target container registry URL
  - name: git-url
    type: string
  - name: git-revision  
    type: string
  - name: git-manifests-url
    type: string
  - name: git-manifests-revision  
    type: string
  - name: git-manifests-username 
    type: string
    description: Git Repo Username for Manifests
  - name: git-manifests-useremail  
    type: string
    description: Git Repo User Email for Manifests
  - name: application-name
    type: string
  - name: revision
    type: string  
  - name: flags
    type: string
  - name: target-url  
    type: string
  - name: zap-scan
    type: string
  - name: zap-options
    type: string
  - name: oc-script
    type: string
  - name: oc-script-2
    type: string
  tasks:
  - name: delete-previous-report    
    taskRef:
      kind: ClusterTask
      name: openshift-client
    params:
    - name: SCRIPT
      value: $(params.oc-script)

  - name: git-clone-health
    runAfter:
      - delete-previous-report
    taskRef:
      name: git-clone
    params:
    - name: url
      value: $(params.git-url)
    - name: revision
      value: $(params.git-revision)
    workspaces:
    - name: output
      workspace: shared-workspace
      
  - name: git-clone-health-manifests
    taskRef:
      name: git-clone
    params:
    - name: url
      value: $(params.git-manifests-url)
    - name: revision
      value: $(params.git-manifests-revision)
    workspaces:
    - name: output
      workspace: manifest-workspace

  - name: scan-app
    taskRef:
      name: sonarqube-scanner
    runAfter:
    - git-clone-health
    - git-clone-health-manifests
    params:
    - name: SONAR_HOST_URL
      value: 'http://sonarqube:9000'
    - name: SONAR_PROJECT_KEY
      value: 'test-project'
    workspaces:
    - name: source-dir
      workspace: shared-workspace
    - name: sonar-settings
      workspace: sonar-settings

  - name: build-container
    taskRef:
      name: s2i-nodejs
      kind: ClusterTask
    runAfter:
    - scan-app
    workspaces:
    - name: source
      workspace: shared-workspace
    params:
    - name: PATH_CONTEXT
      value: $(workspaces.source.path)/$(params.target-path)
    - name: IMAGE
      value: image-registry.openshift-image-registry.svc:5000/app-develop/health-record:$(params.git-revision)

  - name: test-container
    taskRef:
      name: trivy
    runAfter:
    - build-container
    workspaces:
    - name: cache
      workspace: vul-cache
    params:
    - name: IMAGE
      value: image-registry.openshift-image-registry.svc:5000/app-develop/health-record
    - name: IMAGE_DIGEST
      value: $(tasks.build-container.results.IMAGE_DIGEST)

  - name: update-manifests
    taskRef:
      name: kustomize
    runAfter:
    - test-container
    workspaces:
    - name: manifest-dir
      workspace: manifest-workspace
    params:
    - name: KUSTOMIZE_SCRIPT
      value: |
        cd ./demo/deploy/overlays/dev
        kustomize edit set image ___IMAGE_URL___@___IMAGE_DIGEST___=image-registry.openshift-image-registry.svc:5000/app-develop/health-record@$(tasks.build-container.results.IMAGE_DIGEST)
        cd ../stg
        kustomize edit set image ___IMAGE_URL___@___IMAGE_DIGEST___=image-registry.openshift-image-registry.svc:5000/app-develop/health-record@$(tasks.build-container.results.IMAGE_DIGEST)
        cd ../prod
        kustomize edit set image ___IMAGE_URL___@___IMAGE_DIGEST___=image-registry.openshift-image-registry.svc:5000/app-develop/health-record@$(tasks.build-container.results.IMAGE_DIGEST)


  - name: push-manifests
    taskRef:
      name: git-cli
    runAfter:
    - update-manifests
    workspaces:
    - name: source
      workspace: manifest-workspace
    - name: input
      workspace: temp-dir
    params:
    - name: GIT_USER_NAME
      value: $(params.git-manifests-username)
    - name: GIT_USER_EMAIL
      value: $(params.git-manifests-useremail)
    - name: GIT_SCRIPT
      value: |
        cd $(workspaces.source.path)
        git config --global user.email $(GIT_USER_EMAIL) 
        git config --global user.name $(GIT_USER_NAME)
        git checkout main
        git diff
        git add -A ./demo/deploy/overlays/
        git commit -m "[TEKTON-PIPELINES] Change container image in manifests: $(tasks.build-container.results.IMAGE_DIGEST)."
        git push
  
  - name: argo-sync
    taskRef:
      name: argocd-task-sync-and-wait
    runAfter:
    - push-manifests
    params:
    - name: application-name
      value: $(params.application-name)
    - name: revision
      value: $(params.revision)
    - name: flags
      value: $(params.flags)

  - name: dast
    taskRef:
      name: owaspzap
    runAfter:
    - argo-sync
    params:
    - name: ZAP_SCAN
      value: $(params.zap-scan)
    - name: ZAP_OPTIONS
      value: $(params.zap-options)
    - name: TARGET_URL
      value: $(params.target-url)
    workspaces:
    - name: tests
      workspace: shared-workspace
    
  - name: host-dast-report
    runAfter:
      - dast
    taskRef:
      kind: ClusterTask
      name: openshift-client
    params:
    - name: SCRIPT
      value: $(params.oc-script-2)
