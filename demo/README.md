# デモ手順

Container Stater Kitで実施するデモの手順を紹介します。

![overview](./demo-overview.png)

## 1. リポジトリのクローン
---
作成したクラスタに対しocコマンドが実行可能な環境で以下を実行し、必要なリポジトリをクローンします。
```
# マニフェストのみ。アプリ側は不要
git clone https://gitlab.com/k-srkw/starter-kit-cicd-manifest.git
# Fork したものを利用する場合こちらを実行
git clone https://gitlab.com/${GITLAB_USER}/starter-kit-cicd-manifest.git
```

## 2. 実行環境の準備
---
パイプラインを実行するために必要な設定等を行います。
```
cd starter-kit-cicd-manifest/demo/
# Fork していない場合
./demo.sh install --demo
# Fork した場合
./demo.sh install --app-repo https://gitlab.com/${GITLAB_USER}/starter-kit-cicd-app.git --conf-repo https://gitlab.com/${GITLAB_USER}/starter-kit-cicd-manifest.git --demo
```

以下はスクリプト内で実行するため手動でセットアップするときのみ行います。

```
# プロジェクトの作成と変更
oc new-project app-develop
oc label namespace app-develop argocd.argoproj.io/managed-by=openshift-gitops

oc new-project app-staging
oc label namespace app-staging argocd.argoproj.io/managed-by=openshift-gitops

oc new-project app-production
oc label namespace app-production argocd.argoproj.io/managed-by=openshift-gitops

# app-developのコンテナイメージを他のプロジェクトからPull出来るように設定
oc policy add-role-to-user \
    system:image-puller system:serviceaccount:app-staging:default \
    --namespace=app-develop
    
oc policy add-role-to-user \
    system:image-puller system:serviceaccount:app-production:default \
    --namespace=app-develop

# SonarQubeインストール
oc project app-develop
cd tekton/preparations/
oc apply -f sonarqube-install.yaml

# Tekton Pipelines用PVC作成
oc apply -f tekton-pvc.yaml

# AWS用Secret作成
export aws_region=使用するリージョン
export aws_access_key_id=使用するアクセスキー
export aws_secret_access_key=使用するシークレットキー
cat aws-secret.yaml | envsubst |oc apply -f -

# Argo CD ConfigMap作成
export ARGO_SERVER=$(oc get -n openshift-gitops route openshift-gitops-server -o jsonpath={.spec.host})
export ARGO_PASSWORD=$(oc get secret openshift-gitops-cluster -n openshift-gitops -o "jsonpath={.data['admin\.password']}"|base64 --decode)
cat argocd-config.yaml | envsubst |oc apply -f -

# GitLab用Secret作成
export GITLAB_USER=自分のユーザー名
export GITLAB_TOKEN=自分のトークン
cat gitlab-auth.yaml | envsubst | kubectl apply -f -
oc secrets link pipeline gitlab-token

# GitLab用Secret作成（webhook）
export SECRET_TOKEN=Webhook用トークン
oc create secret generic gitlab-webhook --from-literal=secretkey=${SECRET_TOKEN}

# defaultユーザーへのSCC追加
oc adm policy add-scc-to-user anyuid -z default
```

## 3. Argo CDへのリポジトリ追加
---
~~リポジトリをForkした際にPrivateリポジトリとした場合は以下の設定を行います。(Publicの場合不要)~~

Gitea にクローンしたものを利用するため不要。
```
# ログインパスワードの取得
oc get secret openshift-gitops-cluster -n openshift-gitops -o "jsonpath={.data['admin\.password']}"|base64 --decode
```
RouteでURLを確認し、Argo CDコンソールにアクセス。adminでログイン  
設定画面にてConnect repo using HTTPSを選択し、マニフェストリポジトリの情報と認証情報を追加

## 4. Argo CD CRの作成
---
Argo CDによりアプリケーションのデプロイを行うための設定を行います。

以下はスクリプト内で実行するため手動でセットアップするときのみ行います。
```
cd ../../argocd/

# AppProjectの作成
oc apply -f dev-project.yaml
oc apply -f stg-project.yaml
oc apply -f prod-project.yaml

# Applicationの作成 (一度CIを実行するまでは内部レジストリにイメージが無いためアプリはデプロイされません)
oc apply -f dev-app.yaml
oc apply -f stg-app.yaml
oc apply -f prod-app.yaml
```

## 5. Tekton Pipelines CRの作成
---
Tekton PipelinesのCRオブジェクトを作成します。

以下はスクリプト内で実行するため手動でセットアップするときのみ行います。
```
cd ../tekton/tasks/

# Taskの作成
oc apply -f .

# Pipelineの作成(ROSAの場合)
cd ../pipelines/
oc apply -f starterkit-pipeline.yaml

# ROSA以外の環境でデモを実施する場合以下のPipelineを使用して下さい
# (TestCafeによるUIの試験とスクリーンショットのS3へのアップロードなし)
# oc apply -f starterkit-pipeline-no-e2e.yaml

```

## 6. Tekton Triggers CRの作成
---
Tekton TriggersのCRオブジェクトを作成します。

以下はスクリプト内で実行するため手動でセットアップするときのみ行います。
```
cd ../triggers/

# Triggers用のSA作成
oc apply -f sample-sa.yaml

# EventListner/TriggerBinding/TriggerTemplate の作成
export TARGET_URL=http://$(oc get -n app-develop route health-record  -o jsonpath={.spec.host})
cat starterkit-template.yaml | envsubst |oc apply -f -

oc apply -f starterkit-binding.yaml  
oc apply -f starterkit-listner.yaml  

# EventListner用Routeの作成
oc apply -f el-starterkit-route.yaml
```

## 7. GitLab Webhookの設定
---
EventListnerのRouteのURLをコピーし、
starter-kit-cicd-appリポジトリにて Settings -> Webhook を選択し、上記のURLを設定します。  
この時、Secret tokenには`2. 実行環境の準備`で作成したWebhook用トークンの値を入れます。

 TriggerとしてPush eventsにチェックを入れ、対象のブランチをmainとし設定し保存します。

作成したWebhookにて、Test -> Push events を実行し、パイプラインが実行されるか確認しましょう。

## 8. デモでの操作
---
starter-kit-cicd-appリポジトリを開き、featureブランチを選択します。
/site/public/index.html を選択し
WebIDEにて74行目以下のコメントアウトを外します。
```
# before
      <!--
      <div class="box">
        <div class="map" id='map'></div>
      </div>
      -->

# after
      <div class="box">
        <div class="map" id='map'></div>
      </div>
```
featureにて上記の変更をcommitしたら、featureからmainへのMRを作成しマージを行います。  
マージを行うと`7. GitLab Webhookの設定`で設定した内容に従いWebhookが実行されCIパイプラインが実行されます。


OpenShiftコンソールのに画面を移し、パイプライン実行のログからパイプライン実行状況を確認します。  
* SonarQubeにアクセス(admin/admin)し静的診断の結果を説明する
* 実行ログからtrivyの診断結果を見せる
* S3にアクセスしtestcafeで撮影したスクリーンショットを見せる
* 開発環境のアプリケーションにログインした際にマップが追加されていることを確認する
