#!/bin/bash

set -e -u -o pipefail
declare -r SCRIPT_DIR=$(cd -P $(dirname $0) && pwd)
declare PRJ_PREFIX="app"
declare COMMAND="help"
declare -x APP_REPO_NAME="starter-kit-cicd-app"
declare -x CONFIG_REPO_NAME="starter-kit-cicd-manifest"
declare -x APP_REPO="https://gitlab.com/k-srkw/starter-kit-cicd-app.git"
declare -x CONFIG_REPO="https://gitlab.com/k-srkw/starter-kit-cicd-manifest.git"
declare -x DEMO=""

valid_command() {
  local fn=$1; shift
  [[ $(type -t "$fn") == "function" ]]
}

info() {
    printf "\n# INFO: $@\n"
}

err() {
  printf "\n# ERROR: $1\n"
  exit 1
}

while (( "$#" )); do
  case "$1" in
    install|uninstall|start)
      COMMAND=$1
      shift
      ;;
    -p|--project-prefix)
      PRJ_PREFIX=$2
      shift 2
      ;;
    --app-repo-name)
      declare -x APP_REPO_NAME=$2
      shift 2
      ;;
    --app-repo)
      declare -x APP_REPO=$2
      shift 2
      ;;
    --conf-repo-name)
      declare -x CONFIG_REPO_NAME=$2
      shift 2
      ;;
    --conf-repo)
      declare -x CONFIG_REPO=$2
      shift 2
      ;;
    --demo)
      declare -x DEMO="true"
      shift
      ;;
    --)
      shift
      break
      ;;
    -*|--*)
      err "Error: Unsupported flag $1"
      ;;
    *)
      break
  esac
done

declare -r dev_prj="$PRJ_PREFIX-develop"
declare -r stage_prj="$PRJ_PREFIX-staging"
declare -r prod_prj="$PRJ_PREFIX-production"

command.help() {
  cat <<-EOF

  Usage:
      demo [command] [options]

  Example:
      demo install --project-prefix mydemo

  COMMANDS:
      install                        Sets up the demo and creates namespaces
      uninstall                      Deletes the demo
      start                          Starts the deploy DEV pipeline
      help                           Help about this command

  OPTIONS:
      -p|--project-prefix [string]   Prefix to be added to demo project names e.g. PREFIX-dev
      --app-repo-name [string]       Application repository name
      --app-repo [string]            Application repository URL
      --conf-repo-name [string]      Configuration repository name
      --conf-repo [string]           Configuration repository URL
      --demo                         Install demo env
EOF
}

command.install() {
  oc version >/dev/null 2>&1 || err "no oc binary found"

  info "Creating namespaces $dev_prj, $stage_prj, $prod_prj"
  oc get ns $dev_prj 2>/dev/null  || {
    oc new-project $dev_prj
  }
  oc get ns $stage_prj 2>/dev/null  || {
    oc new-project $stage_prj
  }
  oc get ns $prod_prj 2>/dev/null  || {
    oc new-project $prod_prj
  }

  info "Deploying CI/CD infra to $dev_prj namespace"
  oc apply -f infra -n $dev_prj
  declare -x GITEA_HOSTNAME=$(oc get route gitea -o template --template='{{.spec.host}}' -n $dev_prj)

  info "Initiatlizing git repository in Gitea and configuring webhooks"
  cat config/gitea-configmap.yaml | envsubst | oc create -f - -n $dev_prj
  oc rollout status deployment/gitea -n $dev_prj
  cat config/gitea-init-taskrun.yaml | envsubst | oc create -f - -n $dev_prj

  sleep 10

  if [[ $DEMO = "true" ]]; then

  info "Configure service account permissions for pipeline"
  oc policy add-role-to-user system:image-puller system:serviceaccount:$stage_prj:default -n $dev_prj
  oc policy add-role-to-user system:image-puller system:serviceaccount:$prod_prj:default -n $dev_prj

  info "Grants permissions to ArgoCD instances to manage resources in target namespaces"
  oc label ns $dev_prj argocd.argoproj.io/managed-by=openshift-gitops
  oc label ns $stage_prj argocd.argoproj.io/managed-by=openshift-gitops
  oc label ns $prod_prj argocd.argoproj.io/managed-by=openshift-gitops

  info "Deploying pipeline and tasks to $dev_prj namespace"
  # Tekton Pipelines用PVC作成
  oc apply -f tekton/preparations/tekton-pvc.yaml -n $dev_prj

  # AWS用Secret作成
  declare -x aws_region=ap-northeast-1
  declare -x aws_access_key_id=admin
  declare -x aws_secret_access_key=admin
  cat tekton/preparations/aws-secret.yaml | envsubst |oc apply -f -

  # Argo CD ConfigMap作成
  declare -x ARGO_SERVER=$(oc get -n openshift-gitops route openshift-gitops-server -o jsonpath={.spec.host})
  declare -x ARGO_PASSWORD=$(oc get secret openshift-gitops-cluster -n openshift-gitops -o "jsonpath={.data['admin\.password']}"|base64 --decode)
  cat tekton/preparations/argocd-config.yaml | envsubst |oc apply -f - -n $dev_prj

  # GitLab用Secret作成
  declare -x GITLAB_USER=gitea
  declare -x GITLAB_USER_EMAIL=admin@gitea.com
  declare -x GITLAB_TOKEN=openshift
  cat tekton/preparations/gitlab-auth.yaml | envsubst | oc apply -f - -n $dev_prj
  oc secrets link pipeline gitlab-token -n $dev_prj

  # GitLab用Secret作成（webhook）
  declare -x SECRET_TOKEN=openshift
  oc create secret generic gitlab-webhook --from-literal=secretkey=${SECRET_TOKEN} -n $dev_prj

  # defaultユーザーへのSCC追加
  oc adm policy add-scc-to-user anyuid -z default -n $dev_prj

  info "Configure Argo CD"
  cat << EOF > argocd/tmp-argocd-app-patch.yaml
---
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: dev-health
  namespace: openshift-gitops
spec:
  sourceRepos:
  - http://$GITEA_HOSTNAME/gitea/${CONFIG_REPO_NAME}
---
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: stg-health
  namespace: openshift-gitops
spec:
  sourceRepos:
  - http://$GITEA_HOSTNAME/gitea/${CONFIG_REPO_NAME}
---
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: prod-health
  namespace: openshift-gitops
spec:
  sourceRepos:
  - http://$GITEA_HOSTNAME/gitea/${CONFIG_REPO_NAME}
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app-develop
  namespace: openshift-gitops
spec:
  destination:
    namespace: $dev_prj
  source:
    repoURL: http://$GITEA_HOSTNAME/gitea/${CONFIG_REPO_NAME}
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app-staging
  namespace: openshift-gitops
spec:
  destination:
    namespace: $stage_prj
  source:
    repoURL: http://$GITEA_HOSTNAME/gitea/${CONFIG_REPO_NAME}
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app-production
  namespace: openshift-gitops
spec:
  destination:
    namespace: $prod_prj
  source:
    repoURL: http://$GITEA_HOSTNAME/gitea/${CONFIG_REPO_NAME}
EOF
  oc apply -k argocd

  sleep 10

  ## 5. Tekton Pipelines CRの作成
  oc apply -f tekton/tasks -n $dev_prj
  #oc apply -f tekton/pipelines/starterkit-pipeline.yaml
  oc apply -f tekton/pipelines/starterkit-pipeline-no-e2e.yaml -n $dev_prj

  ## 6. Tekton Triggers CRの作成
  # Triggers用のSA作成
  oc apply -f tekton/triggers/sample-sa.yaml -n $dev_prj
  # EventListner/TriggerBinding/TriggerTemplate の作成
  declare -x TARGET_URL=http://$(oc get -n $dev_prj route health-record  -o jsonpath={.spec.host})
  cat tekton/triggers/starterkit-template.yaml | envsubst | oc apply -f - -n $dev_prj

  oc apply -f tekton/triggers/starterkit-binding.yaml -n $dev_prj
  oc apply -f tekton/triggers/starterkit-listner.yaml -n $dev_prj

  # PipelineRun の Git URL 変更
  #cat tekton/pipelines/starterkit-pipelinerun-base.yaml | envsubst > tekton/pipelines/starterkit-pipelinerun-exec.yaml
  cat tekton/pipelines/starterkit-pipelinerun-no-e2e-base.yaml | envsubst > tekton/pipelines/starterkit-pipelinerun-no-e2e-exec.yaml

  # EventListner用Routeの作成
  oc apply -f tekton/triggers/el-starterkit-route.yaml -n $dev_prj
  
  fi

  oc project $dev_prj

  cat <<-EOF

############################################################################
############################################################################

  Demo is installed! Give it a few minutes to finish deployments and then:

  1) Go to App Git repository in Gitea:
     http://$GITEA_HOSTNAME/gitea/$APP_REPO_NAME

  2) Log into Gitea with username/password: gitea/gitea

  3) Edit a file in the repository and commit to trigger the pipeline

  4) Check the pipeline run logs in Dev Console or Tekton CLI:

    \$ tkn pipeline logs starterkit-pipeline -L -f -n $dev_prj


  You can find further details at:

  Gitea Git Server: http://$GITEA_HOSTNAME/explore/repos
  SonarQube: https://$(oc get route sonarqube -o template --template='{{.spec.host}}' -n $dev_prj)
  Argo CD:  https://$(oc get route openshift-gitops-server -o template --template='{{.spec.host}}' -n openshift-gitops)  [login with OpenShift credentials]

############################################################################
############################################################################
EOF
}

command.start() {
  #oc create -f tekton/pipelines/starterkit-pipelinerun.yaml -n $dev_prj
  oc create -f tekton/pipelines/starterkit-pipelinerun-no-e2e-exec.yaml -n $dev_prj
}

command.uninstall() {
  oc delete project $dev_prj $stage_prj $prod_prj
}

main() {
  local fn="command.$COMMAND"
  valid_command "$fn" || {
    err "invalid command '$COMMAND'"
  }

  cd $SCRIPT_DIR
  $fn
  return $?
}

main
